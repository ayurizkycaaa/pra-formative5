public class Formative1 {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            try {
                new ProsesThread().start();
                ProsesThread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class ProsesThread extends Thread {
    @Override
    public void run() {
        System.out.println("Thread " + this.getId() + " is running" );
    }
}
