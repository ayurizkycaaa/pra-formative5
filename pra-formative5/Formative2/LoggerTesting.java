import java.util.Scanner;
import java.util.logging.Logger;

public class LoggerTesting {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger( LoggerTesting.class.getName());
        double angka1, angka2;

        System.out.println("OPERASI PEMBAGIAN");
        try (Scanner keyboard = new Scanner(System.in)) {
            System.out.print("Masukkan Angka 1 : ");
            angka1 = keyboard.nextInt();
            System.out.print("Masukkan Angka 2 : ");
            angka2 = keyboard.nextInt();

            System.out.println("Hasilnya : " + pembagian(angka1, angka2));
            logger.info("Hello INFO " + logger.getName());
        }catch (Exception e) {
            logger.severe("Hello SEVERE " + logger.getName());
        }
        
    }

    static double pembagian(double angka1, double angka2) {
        return angka1 / angka2;
    }
}
